<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/informasi', 'HomeController@informasi')->name('informasi');

Route::get('/', 'InfoController@index')->name('index');
Route::get('/komoditas', 'InfoController@komoditas')->name('komoditas');
Route::get('/business_room', 'InfoController@business_room')->name('business_room');
Route::get('/pameran_virtual', 'InfoController@pameran_virtual')->name('pameran_virtual');
Route::get('/info_pameran','InfoController@info_pameran');

Route::get('/entitas_usaha/{id}', 'InfoController@entitas_usaha')->name('entitas_usaha');
Route::get('/entitas_usaha_detail/{id}', 'InfoController@entitas_usaha_detail')->name('entitas_usaha_detail');

Route::get('/katalog/{id}', 'InfoController@katalog')->name('katalog');
Route::get('/katalog_detail/{id}', 'InfoController@katalog_detail')->name('katalog_detail');
Route::get('/katalog_komoditas/{id}', 'InfoController@katalog_komoditas')->name('katalog_komoditas');

Route::get('/kategori','KategoriController@index');
Route::post('/kategori/store','KategoriController@store');
Route::post('/kategori/detailstore','KategoriController@detailstore');
Route::post('/kategori/detailjenisstore','KategoriController@detailjenisstore');

Route::post('/kategori/update','KategoriController@update');
Route::post('/kategori/updatejenis','KategoriController@updatejenis');
Route::post('/kategori/updatekomoditas','KategoriController@updatekomoditas');

Route::get('/kategori/edit/{id_kategori}','KategoriController@edit');
Route::get('/kategori/editjenis/{id_kategori_jenis}','KategoriController@editjenis');
Route::get('/kategori/editkomoditas/{id_kategori_komoditas}','KategoriController@editkomoditas');

Route::get('/kategori/hapus/{id_kategori}', 'KategoriController@hapus');
Route::get('/kategori/hapusjenis/{id_kategori_jenis}', 'KategoriController@hapusjenis');
Route::get('/kategori/hapuskomoditas/{id_kategori_komoditas}', 'KategoriController@hapuskomoditas');

Route::get('/kategori/detail/{id_kategori}','KategoriController@detail');
Route::get('/kategori/detailjenis/{id_kategori_jenis}','KategoriController@detailjenis');
Route::get('/kategori/detailkomoditas/{id_kategori_komoditas}','KategoriController@detailkomoditas');


Route::get('/entitas','EntitasController@index');
Route::post('/entitas/store','EntitasController@store');
Route::post('/entitas/update','EntitasController@update');
Route::get('/entitas/edit/{id_entitas}','EntitasController@edit');
Route::get('/entitas/hapus/{id_entitas}', 'EntitasController@hapus');


Route::get('/organisasi','OrganisasiController@index');
Route::post('/organisasi/store','OrganisasiController@store');
Route::post('/organisasi/update','OrganisasiController@update');
Route::get('/organisasi/edit/{id_organisasi}','OrganisasiController@edit');
Route::get('/organisasi/detail/{id_organisasi}','OrganisasiController@detail');
Route::get('/organisasi/hapus/{id_organisasi}', 'OrganisasiController@hapus');

Route::get('/user','UserController@index');
Route::post('/user/store','UserController@store');
Route::post('/user/update','UserController@update');
Route::get('/user/edit/{id}','UserController@edit');

Route::get('/pengguna','UserController@index2');
Route::post('/pengguna/store','UserController@store2');
Route::post('/pengguna/update','UserController@update2');
Route::get('/pengguna/edit/{id}','UserController@edit2');

// Route::get('/barang','BarangController@index');
// Route::get('/cetak','BarangController@cetak');
// Route::post('/barang/store','BarangController@store');
// Route::post('/barang/update','BarangController@update');
// Route::get('/barang/edit/{id_barang}','BarangController@edit');

// Route::get('/pasok','PasokController@index');
// Route::post('/pasok/store','PasokController@store');
// Route::post('/pasok/update','PasokController@update');
// Route::get('/pasok/edit/{id_pasok}','PasokController@edit');

Route::get('/eselon','EselonController@index');
Route::post('/eselon/store','EselonController@store');
Route::post('/eselon/update','EselonController@update');
Route::get('/eselon/edit/{id}','EselonController@edit');
Route::get('/eselon/hapus/{id}', 'EselonController@hapus');

// Route::get('/ambil','TransaksiController@ambil');
// Route::get('/ambil2','TransaksiController@ambil2');

// Route::get('/transaksi','TransaksiController@index');

// Route::get('/nyokot','TransaksiController@nyokot');
// Route::get('/nyokot2/{id}','TransaksiController@nyokot2');

// Route::post('/masuk/sementara','TransaksiController@store');
// Route::post('/masuk/semua','TransaksiController@storesemua');
// Route::get('/cetak/{kode_transaksi}','TransaksiController@cetak');

// Route::get('/laporan','LaporanController@index');
// Route::get('/laporan/{kode_transaksi_kembalian}','LaporanController@detail');

Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');

Route::get('/pameran','PameranController@index');
Route::post('/pameran/store','PameranController@store');
Route::post('/pameran/update','PameranController@update');
Route::get('/pameran/edit/{id_pameran}','PameranController@edit');
Route::get('/pameran/hapus/{id_pameran}', 'PameranController@hapus');
Route::get('/pameran/detail/{id_pameran}','PameranController@detail');
