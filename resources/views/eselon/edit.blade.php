@extends('layouts.template')
@section('content')
<title>Eselon | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/kategori/">Eselon</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Eselon</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
    </div>
    <div class="card-body">
        <form action="/eselon/update" method="post">
            @csrf
            <div class="form-group">
                <label for="">Nama Eselon</label>
                <input type="hidden" name="id_eselon" value="{{$eselon->id_eselon}}">
                <input type="text" name="nama_eselon" class="form-control" value="{{$eselon->nama_eselon}}">
            </div>

            <!-- <div class="form-group">
                <label for="">Deksripsi</label>
                <input type="hidden" name="deskripsi" value="{{$eselon->deskripsi}}">
                <input type="text" name="deskripsi" class="form-control" value="{{$eselon->deskripsi}}" required>
            </div> -->
            <input type="submit" value="Update" class="btn btn-warning">
        </form>
    </div>
</div>


@endsection