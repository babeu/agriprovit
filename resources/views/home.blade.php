@extends('layouts.template')
@section('content')
<title>Dashboard | Agriprovit</title>
<style>
#content {
            background-image: url({{url('assets/img/Home/bga.jpg')}});
            background-size: 1055px;
}

.hovereffect {
  width: 100%;
  height: 100%;
  float: left;
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: default;
}

.hovereffect .overlay {
  width: 100%;
  height: 100%;
  position: absolute;
  overflow: hidden;
  top: 0;
  left: 0;
}

.hovereffect img {
  display: block;
  position: relative;
  -webkit-transition: all 0.4s ease-in;
  transition: all 0.4s ease-in;
}

.hovereffect:hover img {
  filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0 0 0 1 0" /><feGaussianBlur stdDeviation="3" /></filter></svg>#filter');
  filter: grayscale(1) blur(3px);
  -webkit-filter: grayscale(1) blur(3px);
  -webkit-transform: scale(1.2);
  -ms-transform: scale(1.2);
  transform: scale(1.2);
}

.hovereffect h2 {
  text-transform: uppercase;
  text-align: center;
  position: relative;
  font-size: 17px;
  padding: 10px;
  background: rgba(0, 0, 0, 0.6);
}

.hovereffect a.info {
  display: inline-block;
  text-decoration: none;
  padding: 7px 14px;
  border: 1px solid #fff;
  margin: 50px 0 0 0;
  background-color: transparent;
}

.hovereffect a.info:hover {
  box-shadow: 0 0 5px #fff;
}

.hovereffect a.info, .hovereffect h2 {
  -webkit-transform: scale(0.7);
  -ms-transform: scale(0.7);
  transform: scale(0.7);
  -webkit-transition: all 0.4s ease-in;
  transition: all 0.4s ease-in;
  opacity: 0;
  filter: alpha(opacity=0);
  color: #fff;
  text-transform: uppercase;
}

.hovereffect:hover a.info, .hovereffect:hover h2 {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
}
</style>
@if( Session::get('berhasil') !="")
<div class='alert alert-success'><center><b>{{Session::get('berhasil')}}</b></center></div>        
@endif
<div class="row">

<!-- Earnings (Monthly) Card Example -->
<!-- <div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Organisasi</div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">{{$jumlah_barang}}</div>
        </div>
        <div class="col-auto">
          <i class="fas fa-calendar fa-2x text-gray-300"></i>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- Earnings (Monthly) Card Example -->
<!-- <div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-success shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Pengguna</div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">{{$jumlah_pasok}}</div>
        </div>
        <div class="col-auto">
          <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- <div class="container">
  <div class="row" style="margin: 15px;">
    <a href="{{ asset('assets/img/1 (1).jpg') }}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
      <img src="{{ asset('assets/img/1 (1).jpg') }}" class="img-fluid rounded">
      
    </a>
    <a href="{{ asset('assets/img/1 (2).jpg') }}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
      <img src="{{ asset('assets/img/1 (2).jpg') }}" class="img-fluid rounded">
      
    </a>
    <a href="{{ asset('assets/img/1 (3).jpg') }}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
      <img src="{{ asset('assets/img/1 (3).jpg') }}" class="img-fluid rounded">
      
    </a>
  </div>
  <div class="row" style="margin: 15px;">
    <a href="{{ asset('assets/img/1 (4).jpg') }}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
      <img src="{{ asset('assets/img/1 (4).jpg') }}" class="img-fluid rounded">
      
    </a>
    <a href="{{ asset('assets/img/1 (5).jpg') }}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
      <img src="{{ asset('assets/img/1 (5).jpg') }}" class="img-fluid rounded">
      
    </a>
    <a href="{{ asset('assets/img/1 (6).jpg') }}" data-toggle="lightbox" data-gallery="gallery" class="col-md-4">
      <img src="{{ asset('assets/img/1 (6).jpg') }}" class="img-fluid rounded">
      
    </a>
  </div>
</div> -->

<div class="container">
<h1 class="bd-title" id="content" style="color: cadetblue;">Apa itu Agriprovit ?</h1>
  <p>Agriprovit adalah Aplikasi Sistim Informasi berbasis jaringan yang <br>digunakan  
  untuk mempromosikan UMKM yang bergerak di bidang <br> 
  Pertanian serta pusat informasi mengenai komoditas pertanian.<br><br>
  <a href="/informasi" class="btn btn-info">Informasi</a>
<!-- Grid row -->
<div class="row">

 


</div> 
<!-- Grid row -->

<!-- Grid row -->

<!-- Grid row -->
</div>
<!-- <div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-dark shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-dark text-uppercase mb-1">Jumlah Transaksi</div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">{{$jumlah_transaksi}}</div>
        </div>
        <div class="col-auto">
          <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
        </div>
      </div>
    </div>
  </div>
</div> -->


<!-- Pending Requests Card Example -->
<!-- <div class="col-xl-3 col-md-6 mb-4">
  <div class="card border-left-warning shadow h-100 py-2">
    <div class="card-body">
      <div class="row no-gutters align-items-center">
        <div class="col mr-2">
          <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah Kasir</div>
          <div class="h5 mb-0 font-weight-bold text-gray-800">{{$jumlah_kasir}}</div>
        </div>
        <div class="col-auto">
          <i class="fas fa-comments fa-2x text-gray-300"></i>
        </div>
      </div>
    </div>
  </div>
</div> -->
</div>

<!-- <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data - data Transaksi</h6>
    </div>
    <div class="card-body">
    <div class="table-responsive">
            <table id="dataTable" class="table table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Transaksi</th>
                        <th>Jumlah Bayar</th>
                        <th>Kembalian</th>
                        <th>Tanggal</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($laporan as $i => $u)
                    <tr>
                        <td>{{++$i}}</td>
                        <td>{{$u->kode_transaksi_kembalian}}</td>
                        <td>{{$u->bayar}}</td>
                        <td>{{$u->kembalian}}</td>
                        <td>{{$u->tanggal_transaksi}}</td>
                        <td><a href="/laporan/{{ $u->kode_transaksi_kembalian}}" class="btn btn-primary btn-sm ml-2">View</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div> -->

<!-- <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Organisasi</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        @if( Session::get('masuk') !="")
            <div class='alert alert-success'><center><b>{{Session::get('masuk')}}</b></center></div>        
            @endif
            @if( Session::get('update') !="")
            <div class='alert alert-success'><center><b>{{Session::get('update')}}</b></center></div>        
            @endif
            
            <br>
            <br>
            <table id="dataTable" class="table table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Organisasi</th>
                        <th>Tipe</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($organisasi as $i => $u)
                    <tr>
                        <td>{{++$i}}</td>
                        <td>{{$u->nama_organisasi}}</td>
                        <td>{{$u->nama_kategori}}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div> -->
    </div>
</div>



<!-- <script>
$(document).on("click", '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});</script> -->


@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(function() {
var selectedClass = "";
$(".filter").click(function(){
selectedClass = $(this).attr("data-rel");
$("#gallery").fadeTo(100, 0.1);
$("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
setTimeout(function() {
$("."+selectedClass).fadeIn().addClass('animation');
$("#gallery").fadeTo(300, 1);
}, 300);
});
});
</script>