@extends('layouts.template')
@section('content')
<title>Informasi | Agriprovit</title>
<style>

.hovereffect {
  width: 100%;
  height: 100%;
  float: left;
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: default;
}

.hovereffect .overlay {
  width: 100%;
  height: 100%;
  position: absolute;
  overflow: hidden;
  top: 0;
  left: 0;
}

.hovereffect img {
  display: block;
  position: relative;
  -webkit-transition: all 0.4s ease-in;
  transition: all 0.4s ease-in;
}

.hovereffect:hover img {
  filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0 0 0 1 0" /><feGaussianBlur stdDeviation="3" /></filter></svg>#filter');
  filter: grayscale(1) blur(3px);
  -webkit-filter: grayscale(1) blur(3px);
  -webkit-transform: scale(1.2);
  -ms-transform: scale(1.2);
  transform: scale(1.2);
}

.hovereffect h2 {
  text-transform: uppercase;
  text-align: center;
  position: relative;
  font-size: 17px;
  padding: 10px;
  background: rgba(0, 0, 0, 0.6);
}

.hovereffect a.info {
  display: inline-block;
  text-decoration: none;
  padding: 7px 14px;
  border: 1px solid #fff;
  margin: 50px 0 0 0;
  background-color: transparent;
}

.hovereffect a.info:hover {
  box-shadow: 0 0 5px #fff;
}

.hovereffect a.info, .hovereffect h2 {
  -webkit-transform: scale(0.7);
  -ms-transform: scale(0.7);
  transform: scale(0.7);
  -webkit-transition: all 0.4s ease-in;
  transition: all 0.4s ease-in;
  opacity: 0;
  filter: alpha(opacity=0);
  color: #fff;
  text-transform: uppercase;
}

.hovereffect:hover a.info, .hovereffect:hover h2 {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
}
</style>
@if( Session::get('berhasil') !="")
<div class='alert alert-success'><center><b>{{Session::get('berhasil')}}</b></center></div>        
@endif
<div class="row">

<div class="container">
<!-- Grid row -->
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/TANAMAN.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Tanaman Pangan</h2>
           <a class="info" href="/kategori/detail/4">Klik Disini</a>
        </div>
    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
        <img class="img-responsive" src="{{ asset('assets/img/Informasi/HORTIKULTURA.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Hortikultra</h2>
           <a class="info" href="/kategori/detail/1">Klik Disini</a>
        </div>
    </div>
</div>


</div>
<br>
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/PERKEBUNAN.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Perkebunan</h2>
           <a class="info" href="/kategori/detail/2">Klik Disini</a>
        </div>
    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/PETERNAKAN.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Peternakan</h2>
           <a class="info" href="/kategori/detail/3">Klik Disini</a>
        </div>
    </div>
</div>

</div>
<br>
<div class="row">

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/PRASARANA.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Prasarana & Sarana Pertanian</h2>
           <a class="info" href="/kategori/detail/5">Klik Disini</a>
        </div>
    </div>
</div>
  
</div> 
<!-- Grid row -->

<!-- Grid row -->

<!-- Grid row -->
</div>

</div>



@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(function() {
var selectedClass = "";
$(".filter").click(function(){
selectedClass = $(this).attr("data-rel");
$("#gallery").fadeTo(100, 0.1);
$("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
setTimeout(function() {
$("."+selectedClass).fadeIn().addClass('animation');
$("#gallery").fadeTo(300, 1);
}, 300);
});
});
</script>