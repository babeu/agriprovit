@extends('layouts.template')
@section('content')
<title>Organisasi | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/organisasi/">Entitas</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Entitas</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detail Data</h6>
    </div>
    <div class="card-body">
    <div class="row">
    <div class="media">
  <img src="{{ asset('images/'.$organisasi->gambar) }}" class="align-self-start mr-3" alt="" title="" style="width: 400px;"></a>
  <div class="media-body">
    <h4 class="mt-0"><b>{{$organisasi->nama_organisasi}}</b></h5>
    <p>Alamat :<br>{{$organisasi->alamat}}</p>
    <p>Telepon / Whatsapp :<br>{{$organisasi->telepon}} / <a href="https://api.whatsapp.com/send?phone={{$organisasi->no_whatsapp}}&text=Halo%20{{$organisasi->nama_organisasi}}..%20Pesan%20Ini%20Dibuat%20Oleh%20Aplikasi%20Agriprovit.id">{{$organisasi->no_whatsapp}}</a></p>
    <p>Website :<br>{{$organisasi->website}}</p>
    <p>Produk :<br>{{$organisasi->produk}}</p>
    <p>Kategori :<br>{{$organisasi->nama_kategori}}</p>
    <p>Entitas Usaha :<br>{{$organisasi->nama_entitas_usaha}}</p>
  </div>
</div>
    </div>
  </div>
  </div>
@endsection