@extends('layouts.template')
@section('content')
<title>Organisasi | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Entitas Usaha</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Entitas Usaha</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        @if( Session::get('masuk') !="")
            <div class='alert alert-success'><center><b>{{Session::get('masuk')}}</b></center></div>        
            @endif
            @if( Session::get('update') !="")
            <div class='alert alert-success'><center><b>{{Session::get('update')}}</b></center></div>        
            @endif
            
            <button class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button>
            
            <br>
            <br>
            <table id="dataTable" class="table table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Entitas Usaha</th>
                        <th>Jenis Entitas</th>
                        <th>Kategori</th>
                        <th style="width: 175.333px;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($organisasi as $i => $u)
                    <tr>
                        <td>{{++$i}}</td>
                        <td>{{$u->nama_organisasi}}</td>
                        <td>{{$u->nama_entitas_usaha}}</td>
                        <td>{{$u->nama_kategori}}</td>
                        <td>
                        <a href="/organisasi/detail/{{ $u->id_organisasi}}" class="btn btn-info btn-sm ml-2">Detail</a>
                        
                        <a href="/organisasi/edit/{{ $u->id_organisasi}}" class="btn btn-primary btn-sm ml-2">Edit</a>
                        <a href="/organisasi/hapus/{{ $u->id_organisasi }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin akan menghapus data ini?');">Hapus</a>
                        
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="tambah" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Masukan Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="/organisasi/store" method="post" enctype="multipart/form-data">
            @csrf
          <div class="form-group">
             <div class="input_fields_wrap">
            
            <div class="form-group">
                <label for="">Nama Organisasi</label>
                <input type="text" name="nama_organisasi" class="form-control" placeholder="Masukan Nama Organisasi" required>
            </div>

            <div class="form-group">
            <label for="">Entitas Usaha</label>
            <select name="entitas_id" id="" class="form-control">
                <option value="" disabled selected>Pilih Entitas</option>
                @foreach($entitas as $e)
                    <option value="{{$e->id_entitas}}">{{$e->nama_entitas_usaha}}</option>
                @endforeach
            </select>
            </div>
            
            <div class="form-group">
            <label for="">Kategori</label>
            <select name="kategori_id" id="" class="form-control">
                <option value="" disabled selected>Pilih Kategori</option>
                @foreach($kategori as $k)
                    <option value="{{$k->id_kategori}}">{{$k->nama_kategori}}</option>
                @endforeach
            </select>
            </div>
            <div class="form-group">
                <label for="">Alamat</label>
                <input type="text" name="alamat" class="form-control" placeholder="Masukan Alamat" required>
            </div>
            <div class="form-group">
                <label for="">Telepon</label>
                <input type="number" name="telepon" class="form-control" placeholder="Masukan Nomor Telepon" required>
            </div>
            <div class="form-group">
                <label for="">Whatsapps</label>
                <input type="text" name="whatsapp" class="form-control" placeholder="Masukan Nomor Whatsapp">
            </div>
            <div class="form-group">
                <label for="">Website</label>
                <input type="text" name="website" class="form-control" placeholder="Masukan Website">
            </div>
            <div class="form-group">
                <label for="">Produk</label>
                <input type="text" name="produk" class="form-control" placeholder="Masukan Produk" required>
            </div>
            <div class="form-group">
                <label for="">Upload Gambar (format : jpg,png) Max : 1 MB</label>
                <input type="file" class="form-control" name="gambar" required> 
            </div>
            
        </div>
              
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="tambahOrganisasi">Simpan</button>
        </form>
        </div>
      </div>
    </div>
  </div>



<!-- 
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 100; //maximum input boxes allowed
        var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment


    $(wrapper).append('<div><table><tr><td><select name="id_kategori[]" id="" class="myselect form-control" required><option selected disabled value="">Pilih Tipe Organisasi</option>@foreach ($kategori as $j)<option value="{{$j->id_kategori}}">{{$j->nama_kategori}}</option>@endforeach</select></div></td><td class="pl-4"><input type="text" name="nama_organisasi" class="form-control" required placeholder="Masukan Organisasi" required></td></tr></table><a href="#" class="remove_field">Remove</a></div>');
    $('.myselect').select2();
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent('div').remove(); x--;

    })
    $('.myselect').select2();
});

</script>
@endpush -->

@endsection