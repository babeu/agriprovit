@extends('layouts.template')
@section('content')
<title>Organisasi | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/organisasi/">Entitas Usaha</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Entitas</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
    </div>
    <div class="card-body">
        <form action="/organisasi/update" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="">Nama Entitas Usaha</label>
                <input type="hidden" name="id_organisasi" value="{{$organisasi->id_organisasi}}">
                <input type="text" name="nama_organisasi" class="form-control" value="{{$organisasi->nama_organisasi}}">
            </div>
            <div class="form-group">
                <label for="">Alamat</label>
                <input type="text" name="alamat" class="form-control"  value="{{$organisasi->alamat}}"  required>
            </div>
            <div class="form-group">
                <label for="">Telepon</label>
                <input type="text" name="telepon" class="form-control"  value="{{$organisasi->telepon}}"  required>
            </div>
            <div class="form-group">
                <label for="">Whatsapp</label>
                <input type="text" name="whatsapp" class="form-control"  value="{{$organisasi->no_whatsapp}}">
            </div>
            <div class="form-group">
                <label for="">Website</label>
                <input type="text" name="website" class="form-control"  value="{{$organisasi->website}}">
            </div>
            <div class="form-group">
                <label for="">Produk</label>
                <input type="text" name="produk" class="form-control"  value="{{$organisasi->produk}}"  required>
            </div>
            <div class="form-group">
            <label for="">Jenis Entitas Usaha</label>
            <select name="entitas_id" id="" class="form-control">
                        <option value="" disabled selected>Pilih Entitas</option>
                    @foreach($entitas as $e)
                    @if ($organisasi->entitas_id == $e->id_entitas)
                        <option value="{{$e->id_entitas}}" selected>{{$e->nama_entitas_usaha}}</option>
                    @else
                        <option value="{{$e->id_entitas}}">{{$e->nama_entitas_usaha}}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
            <label for="">Kategori</label>
            <select name="kategori_id" id="" class="form-control">
                        <option value="" disabled selected>Pilih Kategori</option>
                    @foreach($kategori as $k)
                    @if ($organisasi->kategori_id == $k->id_kategori)
                        <option value="{{$k->id_kategori}}" selected>{{$k->nama_kategori}}</option>
                    @else
                        <option value="{{$k->id_kategori}}">{{$k->nama_kategori}}</option>
                    @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="">Upload Gambar (format : jpg,png) Max : 1 MB</label>
                <input type="file" class="form-control" name="gambar"> 
            </div>
            
            <input type="submit" value="Update" class="btn btn-warning">
        </form>
    </div>
</div>


@endsection