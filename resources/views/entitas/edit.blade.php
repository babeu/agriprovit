@extends('layouts.template')
@section('content')
<title>Kategori | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/entitas/">Jenis Entitas</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Jenis Entitas</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
    </div>
    <div class="card-body">
        <form action="/entitas/update" method="post">
            @csrf
            <div class="form-group">
                <label for="">Jenis Entitas Usaha</label>
                <input type="hidden" name="id_entitas" value="{{$entitas->id_entitas}}">
                <input type="text" name="nama_entitas_usaha" class="form-control" value="{{$entitas->nama_entitas_usaha}}">
            </div>
            <input type="submit" value="Update" class="btn btn-warning">
        </form>
    </div>
</div>


@endsection