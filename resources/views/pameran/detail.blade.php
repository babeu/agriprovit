@extends('layouts.template')
@section('content')
<title>Pameran | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/pameran/">Pameran</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail Pameran</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Detail Data</h6>
    </div>
    <div class="card-body">
    <div class="row">
    <div class="media">
  <img src="{{ asset('images/'.$pameran->gambar) }}" class="align-self-start mr-3" alt="" title="" style="width: 400px;"></a>
  <div class="media-body">
    <h4 class="mt-0"><b>{{$pameran->judul}}</b></h5>
    <p>Tipe :<br>{{$pameran->tipe}}</p>
    <p>Waktu :<br>{{$pameran->waktu}} </p>
    <p>Lokasi :<br>{{$pameran->lokasi}}</p>
    
  </div>
</div>
    </div>
  </div>
  </div>
@endsection