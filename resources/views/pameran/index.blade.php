@extends('layouts.template')
@section('content')
<title>Pameran | Agriprovit</title>
<link href='bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css' rel='stylesheet' type='text/css'>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Pameran</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Pameran</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        @if( Session::get('masuk') !="")
            <div class='alert alert-success'><center><b>{{Session::get('masuk')}}</b></center></div>        
            @endif
            @if( Session::get('update') !="")
            <div class='alert alert-success'><center><b>{{Session::get('update')}}</b></center></div>        
            @endif
            
            <button class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Pameran</button>
            
            <br>
            <br>
            <table id="dataTable" class="table table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Waktu</th>
                        <th>Tipe</th>
                        <th style="width: 175.333px;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pameran as $i => $u)
                    <tr>
                        <td>{{++$i}}</td>
                        <td>{{$u->judul}}</td>
                        <td>{{$u->waktu}}</td>
                        <td>{{$u->tipe}}</td>
                        <td>
                        <a href="/pameran/detail/{{ $u->id_pameran}}" class="btn btn-info btn-sm ml-2">Detail</a>
                        
                        <a href="/pameran/edit/{{ $u->id_pameran}}" class="btn btn-primary btn-sm ml-2">Edit</a>
                        <a href="/pameran/hapus/{{ $u->id_pameran }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin akan menghapus data ini?');">Hapus</a>
                        
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="tambah" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Masukan Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="/pameran/store" method="post" enctype="multipart/form-data">
            @csrf
          <div class="form-group">
             <div class="input_fields_wrap">
            
            <div class="form-group">
                <label for="">Judul</label>
                <input type="text" name="judul" class="form-control" placeholder="Masukan Judul Pameran" required>
            </div>
            <div class="form-group">
            <label for="">Tipe</label>
            <select name="tipe" id="" class="form-control">
                <option value="" disabled selected>Pilih Tipe</option>
                    <option value="offline">Offline</option>
                    <option value="online">Online</option>
            </select>
            </div>
            <div class="form-group">
                <label for="">Waktu</label>
                <input type="datetime-local" name="waktu" class="form-control datepicker" placeholder="Masukan waktu" required>
            </div>
            <div class="form-group">
                <label for="">Lokasi</label>
                <input type="text" name="lokasi" class="form-control" placeholder="Masukan Lokasi Pameran" required>
            </div>
            <div class="form-group">
                <label for="">Upload Gambar (format : jpg,png) Max : 1 MB</label>
                <input type="file" class="form-control" name="gambar" required> 
            </div>
        </div>
              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="tambahOrganisasi">Simpan</button>
        </form>
        </div>
      </div>
    </div>
  </div>

@endsection