@extends('layouts.template')
@section('content')
<title>Pameran | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/pameran/">Pameran</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Pameran</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
    </div>
    <div class="card-body">
        <form action="/pameran/update" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="">Judul</label>
                <input type="hidden" name="id_pameran" value="{{$pameran->id_pameran}}">
                <input type="text" name="judul" class="form-control" value="{{$pameran->judul}}">
            </div>
            <div class="form-group">
            <label for="">Tipe</label>
            <select name="tipe" id="" class="form-control">
                <option value="" disabled selected>Pilih Tipe</option>
                    <option value="offline">Offline</option>
                    <option value="online">Online</option>
            </select>
            </div>
            <div class="form-group">
                <label for="">Waktu</label>
                <input type="text" name="waktu" class="form-control"  value="{{$pameran->waktu}}"  required>
            </div>
            <div class="form-group">
                <label for="">Lokasi</label>
                <input type="text" name="lokasi" class="form-control"  value="{{$pameran->lokasi}}">
            </div>
           
            <div class="form-group">
                <label for="">Upload Gambar (format : jpg,png) Max : 1 MB</label>
                <input type="file" class="form-control" name="gambar"> 
            </div>
            
            <input type="submit" value="Update" class="btn btn-warning">
        </form>
    </div>
</div>


@endsection