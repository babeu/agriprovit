<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Custom styles for this template -->
  <link href="{{url('assets/css/style.default.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <style>
  body {
            background-image: url({{url('assets/img/bgf.jpg')}});
            background-repeat: no-repeat;
            background-size: 1280px;
        }
  </style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-transparent">
  <div class="container">
  <a class="navbar-brand" href="/index">Agriprovit</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto pull-right">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Entitas Usaha
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/entitas_usaha/1">Organisasi Pemerintah</a>
          <a class="dropdown-item" href="/entitas_usaha/2">Asosiasi</a>
          <a class="dropdown-item" href="/entitas_usaha/3">Pelaku Usaha</a>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/komoditas">Komoditas Pertanian</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Business Room
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/info_pameran">Info Pameran</a>
          <a class="dropdown-item" href="/pameran_virtual">Pameran Virtual</a>
          <a class="dropdown-item" href="/business_room">Coming Soon</a>
        </div>
      </li>
    </ul>
    <span class="navbar-text">
      
    </span>
  </div>
  </div>
</nav>

    
<div class="container">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/kategori/">Kategori</a></li>
    <li class="breadcrumb-item"><a href="/kategori/detail/{{$kategori->id_kategori}}">{{$kategori->nama_kategori}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$jenis->nama_kategori_jenis}}</li>
  </ol>
</nav>
<h3 style="color: cadetblue;">{{$jenis->nama_kategori_jenis}}</h3>
<p>{{$jenis->deskripsi}}</p>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Komoditas {{$jenis->nama_kategori_jenis}}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <br>
            <table id="dataTable" class="table table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Komoditas</th>
                        <th>Deskripsi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($komoditas as $i => $u)
                    <tr>
                        <td>{{++$i}}</td>
                        <td><a href="/katalog_komoditas/{{ $u->id_kategori_komoditas}}">{{$u->nama_kategori_komoditas}}</a></td>
                        <td>{{$u->deskripsi}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
    
    <footer id="sticky-footer" class="py-4 bg-light text-black-50">
    <div class="container text-center">
      <small>Copyright &copy; Kementerian Pertanian</small>
    </div>
  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>
