<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Custom styles for this template -->
  <link href="{{url('assets/css/style.default.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <style rel="stylesheet">
  body {
            background-image: url({{url('assets/img/bg-komoditas.jpg')}});
            background-repeat: no-repeat;
            background-size: 1280px;
        }
        .hovereffect {
  width: 100%;
  height: 100%;
  float: left;
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: default;
}

.hovereffect .overlay {
  width: 100%;
  height: 100%;
  position: absolute;
  overflow: hidden;
  top: 0;
  left: 0;
}

.hovereffect img {
  display: block;
  position: relative;
  -webkit-transition: all 0.4s ease-in;
  transition: all 0.4s ease-in;
}

.hovereffect:hover img {
  filter: url('data:image/svg+xml;charset=utf-8,<svg xmlns="http://www.w3.org/2000/svg"><filter id="filter"><feColorMatrix type="matrix" color-interpolation-filters="sRGB" values="0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0 0 0 1 0" /><feGaussianBlur stdDeviation="3" /></filter></svg>#filter');
  filter: grayscale(1) blur(3px);
  -webkit-filter: grayscale(1) blur(3px);
  -webkit-transform: scale(1.2);
  -ms-transform: scale(1.2);
  transform: scale(1.2);
}

.hovereffect h2 {
  text-transform: uppercase;
  text-align: center;
  position: relative;
  font-size: 17px;
  padding: 10px;
  background: rgba(0, 0, 0, 0.6);
}

.hovereffect a.info {
  display: inline-block;
  text-decoration: none;
  padding: 7px 14px;
  border: 1px solid #fff;
  margin: 50px 0 0 0;
  background-color: transparent;
}

.hovereffect a.info:hover {
  box-shadow: 0 0 5px #fff;
}

.hovereffect a.info, .hovereffect h2 {
  -webkit-transform: scale(0.7);
  -ms-transform: scale(0.7);
  transform: scale(0.7);
  -webkit-transition: all 0.4s ease-in;
  transition: all 0.4s ease-in;
  opacity: 0;
  filter: alpha(opacity=0);
  color: #fff;
  text-transform: uppercase;
}

.hovereffect:hover a.info, .hovereffect:hover h2 {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
}
  </style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-transparent">
  <div class="container">
  <a class="navbar-brand" href="/" style="color: darkcyan;">Agriprovit</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto pull-right">
     
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Entitas Usaha
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/entitas_usaha/1">Organisasi Pemerintah</a>
          <a class="dropdown-item" href="/entitas_usaha/2">Asosiasi</a>
          <a class="dropdown-item" href="/entitas_usaha/3">Pelaku Usaha</a>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/komoditas">Komoditas Pertanian</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Business Room
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/info_pameran">Info Pameran</a>
          <a class="dropdown-item" href="/pameran_virtual">Pameran Virtual</a>
          <a class="dropdown-item" href="/business_room">Coming Soon</a>
        </div>
      </li>
    </ul>
    <span class="navbar-text">
        
    </span>
  </div>
  </div>
</nav>

  
    <div class="container">
    
    <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/TANAMAN.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Tanaman Pangan</h2>
           <a class="info" href="/katalog/4">Klik Disini</a>
        </div>
    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
        <img class="img-responsive" src="{{ asset('assets/img/Informasi/HORTIKULTURA.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Hortikultra</h2>
           <a class="info" href="/katalog/1">Klik Disini</a>
        </div>
    </div>
</div>


</div>
<br>
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/PERKEBUNAN.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Perkebunan</h2>
           <a class="info" href="/katalog/2">Klik Disini</a>
        </div>
    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/PETERNAKAN.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Peternakan</h2>
           <a class="info" href="/katalog/3">Klik Disini</a>
        </div>
    </div>
</div>


</div>
<br>
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="hovereffect">
    <img class="img-responsive" src="{{ asset('assets/img/Informasi/PRASARANA.jpg') }}" alt="" style="widht:470px;height:220px;">
        <div class="overlay">
           <h2>Prasarana & Sarana Pertanian</h2>
           <a class="info" href="/katalog/5">Klik Disini</a>
        </div>
    </div>
</div>
    </div>
    </div>
    <br>
    <footer id="sticky-footer" class="py-4 bg-light text-black-50">
    <div class="container text-center">
      <small>Copyright &copy; Kementerian Pertanian</small>
    </div>
  </footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
<script>
$(function() {
var selectedClass = "";
$(".filter").click(function(){
selectedClass = $(this).attr("data-rel");
$("#gallery").fadeTo(100, 0.1);
$("#gallery div").not("."+selectedClass).fadeOut().removeClass('animation');
setTimeout(function() {
$("."+selectedClass).fadeIn().addClass('animation');
$("#gallery").fadeTo(300, 1);
}, 300);
});
});
</body>
</html>
