@extends('layouts.template')
@section('content')
<title>Data Pengguna | Pengguna </title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/pengguna/">User Pengguna</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit User</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
    </div>
    <div class="card-body">
        <form action="/pengguna/update" method="post">
            @csrf
            <div class="form-group">
                <label for="">Nama Pengguna</label>
                <input type="hidden" name="id" value="{{$pengguna->id}}">
                <input type="text" name="name" class="form-control" value="{{$pengguna->name}}">
            </div>
            <div class="form-group">
            <label for="">Kategori Eselon</label>
            <select name="eselon_id" id="" class="form-control">
                        <option value="" disabled selected>Pilih Eselon</option>
                    @foreach($eselon as $e)
                    @if ($pengguna->eselon_id == $e->id_eselon)
                        <option value="{{$e->id_eselon}}" selected>{{$e->nama_eselon}}</option>
                    @else
                        <option value="{{$e->id_eselon}}">{{$e->nama_eselon}}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <input type="submit" value="Update" class="btn btn-warning">
        </form>
    </div>
</div>


@endsection