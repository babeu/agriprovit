<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Custom styles for this template -->
  <link href="{{url('assets/css/style.default.css')}}" rel="stylesheet">
  <link href="{{url('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <style rel="stylesheet">
  body {
            background-image: url({{url('assets/img/bgb.jpg')}});
            background-repeat: no-repeat;
            background-size: 1280px;
        }
      
  </style>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-transparent">
  <div class="container">
  <a class="navbar-brand" href="#" style="color: darkcyan;">Agriprovit</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto pull-right">
     
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Entitas Usaha
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/entitas_usaha/1">Organisasi Pemerintah</a>
          <a class="dropdown-item" href="/entitas_usaha/2">Asosiasi</a>
          <a class="dropdown-item" href="/entitas_usaha/3">Pelaku Usaha</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/komoditas">Komoditas Pertanian</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Business Room
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="/info_pameran">Info Pameran</a>
          <a class="dropdown-item" href="/pameran_virtual">Pameran Virtual</a>
          <a class="dropdown-item" href="/business_room">Coming Soon</a>
        </div>
      </li>
    </ul>
    <span class="navbar-text">
        <a class="nav-link" href="/login">Login</a>
    </span>
  </div>
  </div>
</nav>

    <section class="hero bg-top" id="hero">
      <div class="container">
        <div class="row py-5">
          <div class="col-lg-5 py-5">
           <img src = "{{ asset('assets/img/Logo.png') }}" style="width: 330px;" />
            <img src = "{{ asset('assets/img/logo-kementan.png') }}" style="width: 230px;" />
            
            <p class="my-4 text-muted">
            Aplikasi informasi dan promosi virtual produk pertanian
beserta pelaku usaha serta sarana edukasi publik yang
dilakukan secara digital.</p>
            <ul class="list-inline mb-0">
             
            </ul>
          </div>
          <div class="col-lg-6 ml-auto">
            
          </div>
        </div>
      </div>
    </section>
    <br>
   
    </div>
    <br>
    <footer id="sticky-footer" class="py-4 bg-light text-black-50">
    <div class="container text-center">
      <small>Copyright &copy; Kementerian Pertanian</small>
    </div>
  </footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
$('.navbar-light .dmenu').hover(function () {
        $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
    }, function () {
        $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
    });
});
</script>

</body>
</html>
