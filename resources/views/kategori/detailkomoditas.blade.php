@extends('layouts.template')
@section('content')
<title>Detail Komoditas | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/kategori/">Kategori</a></li>
    <li class="breadcrumb-item"><a href="/kategori/detail/{{$kategori->id_kategori}}">{{$kategori->nama_kategori}}</a></li>
    <li class="breadcrumb-item"><a href="/kategori/detailjenis/{{$jenis->id_kategori_jenis}}">{{$jenis->nama_kategori_jenis}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Detail {{$komoditas->nama_kategori_komoditas}}</li>
  </ol>
</nav>
<div class="media">
  <img class="align-self-start mr-3" src="{{ asset('images/'.$komoditas->gambar) }}" alt="Generic placeholder image">
  <div class="media-body">
    <h3 style="color: cadetblue;">{{$komoditas->nama_kategori_komoditas}}</h3>
    <p>{{$komoditas->deskripsi}}</p>
  </div>
</div>





@endsection