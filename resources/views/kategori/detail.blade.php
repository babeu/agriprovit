@extends('layouts.template')
@section('content')
<title>Detail Kategori | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/kategori/">Kategori</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$kategori->nama_kategori}}</li>
  </ol>
</nav>

<h3 style="color: cadetblue;">{{$kategori->nama_kategori}}</h3>
<p>{{$kategori->deskripsi}}</p>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Jenis {{$kategori->nama_kategori}}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        @if( Session::get('masuk') !="")
            <div class='alert alert-success'><center><b>{{Session::get('masuk')}}</b></center></div>        
            @endif
            @if( Session::get('update') !="")
            <div class='alert alert-success'><center><b>{{Session::get('update')}}</b></center></div>        
            @endif
            <button class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button>
            
            <br>
            <br>
            <table id="dataTable" class="table table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Jenis</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($jenis as $i => $u)
                    <tr>
                        <td>{{++$i}}</td>
                        <td>{{$u->nama_kategori_jenis}}</td>
                        <td>
                        <a href="/kategori/detailjenis/{{ $u->id_kategori_jenis}}" class="btn btn-info btn-sm ml-2">Detail</a>
                        
                        
                        <a href="/kategori/editjenis/{{ $u->id_kategori_jenis}}" class="btn btn-primary btn-sm ml-2">Edit</a>
                        <a href="/kategori/hapusjenis/{{ $u->id_kategori_jenis }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin akan menghapus data ini?');">Hapus</a></td>
                        
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="tambah" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Masukan Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
    <form action="/kategori/detailstore" method="post">
        {{ csrf_field() }}
        
        <div class="form-group">
            <label for="">Jenis</label>
            <input type="hidden" name="id_kategori" value="{{$kategori->id_kategori}}">
            <input type="text" name="nama_kategori_jenis" class="form-control"  required>
        </div>
        <div class="form-group">
            <label for="">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" rows="4" required></textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
    </div>
    </div>
</div>
</div>
@endsection