@extends('layouts.template')
@section('content')
<title>Komoditas | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/kategori/">Kategori</a></li>
    <li class="breadcrumb-item"><a href="/kategori/detail/{{$kategori->id_kategori}}">{{$kategori->nama_kategori}}</a></li>
    <li class="breadcrumb-item"><a href="/kategori/detailjenis/{{$jenis->id_kategori_jenis}}">{{$jenis->nama_kategori_jenis}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit {{$komoditas->nama_kategori_komoditas}}</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
    </div>
    <div class="card-body">
        <form action="/kategori/updatekomoditas" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="">Nama Komoditas</label>
                <input type="hidden" name="id_kategori_jenis" value="{{$komoditas->kategori_jenis_id}}">
                <input type="hidden" name="id_kategori_komoditas" value="{{$komoditas->id_kategori_komoditas}}">
                <input type="text" name="nama_kategori_komoditas" class="form-control" value="{{$komoditas->nama_kategori_komoditas}}">
            </div>

            <div class="form-group">
                <label for="">Deksripsi</label>
                <input type="hidden" name="deskripsi" value="{{$komoditas->deskripsi}}">
                <input type="text" name="deskripsi" class="form-control" value="{{$komoditas->deskripsi}}" required>
            </div>

            <div class="form-group">
                <label for="">Upload Gambar</label>
                <input type="file" class="form-control" name="gambar"> 
            </div>
            <input type="submit" value="Update" class="btn btn-warning">
        </form>
    </div>
</div>


@endsection