@extends('layouts.template')
@section('content')
<title>Detail Jenis | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/kategori/">Kategori</a></li>
    <li class="breadcrumb-item"><a href="/kategori/detail/{{$kategori->id_kategori}}">{{$kategori->nama_kategori}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{$jenis->nama_kategori_jenis}}</li>
  </ol>
</nav>
<h3 style="color: cadetblue;">{{$jenis->nama_kategori_jenis}}</h3>
<p>{{$jenis->deskripsi}}</p>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Komoditas {{$jenis->nama_kategori_jenis}}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        @if( Session::get('masuk') !="")
            <div class='alert alert-success'><center><b>{{Session::get('masuk')}}</b></center></div>        
            @endif
            @if( Session::get('update') !="")
            <div class='alert alert-success'><center><b>{{Session::get('update')}}</b></center></div>        
            @endif
            <button class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button>
            
            <br>
            <br>
            <table id="dataTable" class="table table-bordered" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Komoditas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($komoditas as $i => $u)
                    <tr>
                        <td>{{++$i}}</td>
                        <td>{{$u->nama_kategori_komoditas}}</td>
                        <td>
                        <a href="/kategori/detailkomoditas/{{ $u->id_kategori_komoditas}}" class="btn btn-info btn-sm ml-2">Detail</a>
                  
                        
                        <a href="/kategori/editkomoditas/{{ $u->id_kategori_komoditas}}" class="btn btn-primary btn-sm ml-2">Edit</a>
                        <a href="/kategori/hapuskomoditas/{{ $u->id_kategori_komoditas }}" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin akan menghapus data ini?');">Hapus</a></td>
                  
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="tambah" class="modal fade" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Masukan Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
    <form action="/kategori/detailjenisstore" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        
        <div class="form-group">
            <label for="">Komoditas</label>
            <input type="hidden" name="id_kategori_jenis" value="{{$jenis->id_kategori_jenis}}">
            <input type="text" name="nama_kategori_komoditas" class="form-control"  required>
        </div>
        <div class="form-group">
            <label for="">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" rows="4" required></textarea>
        </div>
        <div class="form-group">
            <label for="">Upload Gambar</label>
            <input type="file" class="form-control" name="gambar" required> 
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
    </div>
    </div>
</div>
</div>
@endsection