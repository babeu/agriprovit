@extends('layouts.template')
@section('content')
<title>Jenis | Agriprovit</title>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/kategori/">Kategori</a></li>
    <li class="breadcrumb-item"><a href="/kategori/detail/{{$kategori->id_kategori}}">{{$kategori->nama_kategori}}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit {{$kategori->nama_kategori}}</li>
  </ol>
</nav>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Edit Data</h6>
    </div>
    <div class="card-body">
        <form action="/kategori/updatejenis" method="post">
            @csrf
            <div class="form-group">
                <label for="">Nama Kategori Jenis</label>
                <input type="hidden" name="id_kategori_jenis" value="{{$jenis->id_kategori_jenis}}">
                <input type="hidden" name="kategori_id" value="{{$jenis->kategori_id}}">
                <input type="text" name="nama_kategori_jenis" class="form-control" value="{{$jenis->nama_kategori_jenis}}">
            </div>

            <div class="form-group">
                <label for="">Deksripsi</label>
                <input type="hidden" name="deskripsi" value="{{$jenis->deskripsi}}">
                <input type="text" name="deskripsi" class="form-control" value="{{$jenis->deskripsi}}" required>
            </div>
            <input type="submit" value="Update" class="btn btn-warning">
        </form>
    </div>
</div>


@endsection