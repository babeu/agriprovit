<?php


namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class InfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function komoditas()
    {
        return view('info/komoditas');
    }

    public function business_room()
    {
        return view('info/business_room');
    }

    public function pameran_virtual()
    {
        return view('info/pameran_virtual');
    }

    public function entitas_usaha($id){

        $organisasi= DB::table('tb_organisasi')
                ->join('kategori', function ($join) {
                    $join->on('tb_organisasi.kategori_id', '=', 'kategori.id_kategori');
                })
                ->leftJoin('entitas_usaha', function ($join) {
                    $join->on('tb_organisasi.entitas_id', '=', 'entitas_usaha.id_entitas');
                })
                ->where('entitas_id',$id)
                ->get();

        $kategori = DB::table('kategori')->get();
        $entitas = DB::table('entitas_usaha')->where('id_entitas',$id)->first();

        return view('info/entitas_usaha',compact('organisasi','kategori','entitas'));
    }

    public function entitas_usaha_detail($id){

        $organisasi= DB::table('tb_organisasi')->where('id_organisasi',$id)
                ->join('kategori',function($join){
                    $join->on('tb_organisasi.kategori_id','=','kategori.id_kategori');
                })
                ->leftJoin('entitas_usaha',function($join){
                    $join->on('tb_organisasi.entitas_id','=','entitas_usaha.id_entitas');
                })->first();

        $kategori = DB::table('kategori')->get();
        $entitas = DB::table('entitas_usaha')->get();

        return view('info/entitas_usaha_detail',compact('organisasi','kategori','entitas'));
    }

    public function katalog($id)
    {
        $kategori= DB::table('kategori')->where('id_kategori',$id)->first();
        $jenis= DB::table('kategori_jenis')->where('kategori_id',$id)->get();
        return view('info/katalog',compact('kategori','jenis'));
    }

    public function katalog_detail($id)
    {
        $jenis= DB::table('kategori_jenis')->where('id_kategori_jenis',$id)->first();
        $kategori = DB::table('kategori')->where('id_kategori',$jenis->kategori_id)->first();
        $komoditas= DB::table('kategori_komoditas')->where('kategori_jenis_id',$id)->get();
        return view('info/katalog_detail',compact('jenis','komoditas','kategori'));
    }

    public function katalog_komoditas($id)
    {
        $komoditas= DB::table('kategori_komoditas')->where('id_kategori_komoditas',$id)->first();
        $jenis = DB::table('kategori_jenis')->where('id_kategori_jenis',$komoditas->kategori_jenis_id)->first();
        $kategori = DB::table('kategori')->where('id_kategori',$jenis->kategori_id)->first();
        return view('info/katalog_komoditas',compact('komoditas','jenis','kategori'));
    }

    public function info_pameran()
    {
        $pameran= DB::table('tb_pameran')->get();

        return view('info/info_pameran',compact('pameran'));
    }
}
