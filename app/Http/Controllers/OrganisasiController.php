<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
//use Youtube;

class OrganisasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $organisasi= DB::table('tb_organisasi')
                ->join('kategori', function ($join) {
                    $join->on('tb_organisasi.kategori_id', '=', 'kategori.id_kategori');
                })
                ->leftJoin('entitas_usaha', function ($join) {
                    $join->on('tb_organisasi.entitas_id', '=', 'entitas_usaha.id_entitas');
                })
                ->orderBy('nama_organisasi','ASC')
                ->get();

        $kategori = DB::table('kategori')->get();
        $entitas = DB::table('entitas_usaha')->get();

        return view('organisasi/index',compact('organisasi','kategori','entitas'));
    }

    public function store(Request $request)
    {
        $cek = DB::table('tb_organisasi')->where('id_organisasi',$request->id_organisasi)->count();
        if($cek == 1){
            return redirect()->back();
        }
        else
        {
            if ($request->hasFile('gambar')) {
                $file = $request->file('gambar');
                $destinationPath = \base_path() ."/public/images";
                $originalFile = $file->getClientOriginalName();
                $file->move($destinationPath, $originalFile);

                    DB::table('tb_organisasi')->insert([
                    'id_organisasi' => $request->id_organisasi,
                    'nama_organisasi' => $request->nama_organisasi,
                    'kategori_id' => $request->kategori_id,
                    'entitas_id' => $request->entitas_id,
                    'alamat' => $request->alamat,
                    'produk' => $request->produk,
                    'telepon' => $request->telepon,
                    'no_whatsapp' => $request->whatsapp,
                    'website' => $request->website,
                    'gambar' => $originalFile
                ]);

                return redirect()->back()->with('masuk','Data Berhasil Di Input');
            }
            else{
                
                DB::table('tb_organisasi')->insert([
                    'id_organisasi' => $request->id_organisasi,
                    'nama_organisasi' => $request->nama_organisasi,
                    'kategori_id' => $request->kategori_id,
                    'entitas_id' => $request->entitas_id,
                    'alamat' => $request->alamat,
                    'produk' => $request->produk,
                    'telepon' => $request->telepon,
                    'no_whatsapp' => $request->whatsapp,
                    'website' => $request->website
                ]);

                return redirect()->back()->with('masuk','Data Berhasil Di Input');
            }
        
        }
        
    }

    public function edit($id)
    {
        $organisasi= DB::table('tb_organisasi')->where('id_organisasi',$id)
                ->join('kategori',function($join){
                    $join->on('tb_organisasi.kategori_id','=','kategori.id_kategori');
                })
                ->leftJoin('entitas_usaha', function ($join) {
                    $join->on('tb_organisasi.entitas_id', '=', 'entitas_usaha.id_entitas');
                })->first();
        $kategori = DB::table('kategori')->get();
        $entitas = DB::table('entitas_usaha')->get();

        return view('organisasi/edit',compact('organisasi','kategori','entitas'));
    }

    public function update(Request $request)
    {

            if ($request->hasFile('gambar')) {
                $file = $request->file('gambar');
                $destinationPath = \base_path() ."/public/images";
                $originalFile = $file->getClientOriginalName();
                $file->move($destinationPath, $originalFile);

                DB::table('tb_organisasi')->where('id_organisasi',$request->id_organisasi)->update([
                    'id_organisasi' => $request->id_organisasi,
                    'nama_organisasi' => $request->nama_organisasi,
                    'kategori_id' => $request->kategori_id,
                    'entitas_id' => $request->entitas_id,
                    'alamat' => $request->alamat,
                    'produk' => $request->produk,
                    'telepon' => $request->telepon,
                    'no_whatsapp' => $request->whatsapp,
                    'website' => $request->website,
                    'gambar' => $originalFile
                ]);
            }
            else{
                DB::table('tb_organisasi')->where('id_organisasi',$request->id_organisasi)->update([
                    'id_organisasi' => $request->id_organisasi,
                    'nama_organisasi' => $request->nama_organisasi,
                    'entitas_id' => $request->entitas_id,
                    'kategori_id' => $request->kategori_id,
                    'alamat' => $request->alamat,
                    'produk' => $request->produk,
                    'telepon' => $request->telepon,
                    'no_whatsapp' => $request->whatsapp
                ]);
            }

        return redirect('organisasi')->with('update','Data Berhasil Di Update');
        
    }

    public function detail($id)
    {
        $organisasi= DB::table('tb_organisasi')->where('id_organisasi',$id)
                ->join('kategori',function($join){
                    $join->on('tb_organisasi.kategori_id','=','kategori.id_kategori');
                })
                ->leftJoin('entitas_usaha',function($join){
                    $join->on('tb_organisasi.entitas_id','=','entitas_usaha.id_entitas');
                })->first();

        $kategori = DB::table('kategori')->get();
        $entitas = DB::table('entitas_usaha')->get();
        return view('organisasi/detail',compact('organisasi','kategori','entitas'));
    }

    public function hapus($id)
    {
        $organisasi = DB::table('tb_organisasi')->where('id_organisasi',$id);
    	$organisasi->delete();
 
    	return redirect('organisasi')->with('update','Data Berhasil Di Hapus');
    }
}
