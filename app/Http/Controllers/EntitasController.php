<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class EntitasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $entitas= DB::table('entitas_usaha')->get();

        return view('entitas/index',compact('entitas'));
    }

    public function store(Request $request)
    {
        DB::table('entitas_usaha')->insert([
            'nama_entitas_usaha' => $request->nama_entitas_usaha
        ]);

        return redirect()->back()->with('masuk','Data Berhasil Di Input');
    }

    public function edit($id)
    {
        $entitas = DB::table('entitas_usaha')->where('id_entitas',$id)->first();

        return view('entitas/edit',compact('entitas'));
    }

    public function update(Request $request)
    {
        DB::table('entitas_usaha')->where('id_entitas',$request->id_entitas)->update([
            'nama_entitas_usaha' => $request->nama_entitas_usaha
        ]);

        return redirect('entitas')->with('update','Data Berhasil Di Update');
    }

    public function hapus($id)
    {
        $entitas = DB::table('entitas_usaha')->where('id_entitas',$id);
    	$entitas->delete();
 
    	return redirect('entitas')->with('update','Data Berhasil Di Hapus');
    }
}
