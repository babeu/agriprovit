<?php


namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $organisasi= DB::table('tb_organisasi')
                ->join('kategori', function ($join) {
                    $join->on('tb_organisasi.kategori_id', '=', 'kategori.id_kategori');
                })->get();

        $kategori = DB::table('kategori')->get();

         $jumlah_barang =  DB::table('tb_barang')->sum('jumlah_barang');
         $jumlah_pasok =  DB::table('tb_pasok')->sum('jumlah_pasok');
         $jumlah_kasir =  DB::table('users')->where('level','K')->count();
         $jumlah_transaksi =  DB::table('tb_kembalian')->count();
         $laporan= DB::table('tb_kembalian')->get();
                    
        return view('home',compact('jumlah_barang','jumlah_pasok','jumlah_transaksi','jumlah_kasir','laporan','organisasi','kategori'));
    
    }

    public function informasi(){
        return view('informasi');
    }
}
