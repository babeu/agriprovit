<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class EselonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $eselon= DB::table('eselon')->get();

        return view('eselon/index',compact('eselon'));
    }

    public function store(Request $request)
    {
        DB::table('eselon')->insert([
            'nama_eselon' => $request->nama_eselon
        ]);

        return redirect()->back()->with('masuk','Data Berhasil Di Input');
    }

    public function edit($id)
    {
        $eselon = DB::table('eselon')->where('id_eselon',$id)->first();

        return view('eselon/edit',compact('eselon'));
    }

    public function update(Request $request)
    {
        DB::table('eselon')->where('id_eselon',$request->id_eselon)->update([
            'nama_eselon' => $request->nama_eselon
        ]);

        return redirect('eselon')->with('update','Data Berhasil Di Update');
    }

    public function hapus($id)
    {
        $eselon = DB::table('eselon')->where('id_eselon',$id);
    	$eselon->delete();
 
    	return redirect('eselon')->with('update','Data Berhasil Di Hapus');
    }
}
