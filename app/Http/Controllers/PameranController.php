<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class PameranController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pameran= DB::table('tb_pameran')->get();

        return view('pameran/index',compact('pameran'));
    }

    public function store(Request $request)
    {
        $cek = DB::table('tb_pameran')->where('id_pameran',$request->id_pameran)->count();
        if($cek == 1){
            return redirect()->back();
        }
        else
        {
            if ($request->hasFile('gambar')) {
                $file = $request->file('gambar');
                $destinationPath = \base_path() ."/public/images";
                $originalFile = $file->getClientOriginalName();
                $file->move($destinationPath, $originalFile);

                    DB::table('tb_pameran')->insert([
                    'judul' => $request->judul,
                    'waktu' => $request->waktu,
                    'lokasi' => $request->lokasi,
                    'tipe' => $request->tipe,
                    'gambar' => $originalFile
                ]);

                return redirect()->back()->with('masuk','Data Berhasil Di Input');
            }
            else{
                DB::table('tb_pameran')->insert([
                    'judul' => $request->judul,
                    'waktu' => $request->waktu,
                    'lokasi' => $request->lokasi,
                    'tipe' => $request->tipe
                ]);

                return redirect()->back()->with('masuk','Data Berhasil Di Input');
            }
        
        }

        return redirect()->back()->with('masuk','Data Berhasil Di Input');
    }

    public function edit($id)
    {
        $pameran = DB::table('tb_pameran')->where('id_pameran',$id)->first();

        return view('pameran/edit',compact('pameran'));
    }

    public function update(Request $request)
    {
        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $destinationPath = \base_path() ."/public/images";
            $originalFile = $file->getClientOriginalName();
            $file->move($destinationPath, $originalFile);

            DB::table('tb_pameran')->where('id_pameran',$request->id_pameran)->update([
                'judul' => $request->judul,
                'waktu' => $request->waktu,
                'lokasi' => $request->lokasi,
                'tipe' => $request->tipe,
                'gambar' => $originalFile
            ]);
        }
        else{
            DB::table('tb_pameran')->where('id_pameran',$request->id_pameran)->update([
                'judul' => $request->judul,
                'waktu' => $request->waktu,
                'lokasi' => $request->lokasi,
                'tipe' => $request->tipe
            ]);
        }

        return redirect('pameran')->with('update','Data Berhasil Di Update');
    }

    public function hapus($id)
    {
        $pameran = DB::table('tb_pameran')->where('id_pameran',$id);
    	$pameran->delete();
 
    	return redirect('pameran')->with('update','Data Berhasil Di Hapus');
    }

    public function detail($id)
    {
        $pameran= DB::table('tb_pameran')->where('id_pameran',$id)->first();

        return view('pameran/detail',compact('pameran'));
    }
}
