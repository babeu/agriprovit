<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $kategori= DB::table('kategori')->get();

        return view('kategori/index',compact('kategori'));
    }

    public function store(Request $request)
    {
        DB::table('kategori')->insert([
            'nama_kategori' => $request->nama_kategori,
            'deskripsi' => $request->deskripsi
        ]);

        return redirect()->back()->with('masuk','Data Berhasil Di Input');
    }

    public function edit($id)
    {
        $kategori = DB::table('kategori')->where('id_kategori',$id)->first();

        return view('kategori/edit',compact('kategori'));
    }

    public function update(Request $request)
    {
        DB::table('kategori')->where('id_kategori',$request->id_kategori)->update([
            'nama_kategori' => $request->nama_kategori,
            'deskripsi' => $request->deskripsi
        ]);

        return redirect('kategori')->with('update','Data Berhasil Di Update');
    }

    public function hapus($id)
    {
        $kategori = DB::table('kategori')->where('id_kategori',$id);
    	$kategori->delete();
 
    	return redirect('kategori')->with('update','Data Berhasil Di Hapus');
    }

    public function detail($id)
    {
        $kategori= DB::table('kategori')->where('id_kategori',$id)->first();
        $jenis= DB::table('kategori_jenis')->where('kategori_id',$id)->get();
        return view('kategori/detail',compact('kategori','jenis'));
    }

    public function detailstore(Request $request)
    {
        DB::table('kategori_jenis')->insert([
            'nama_kategori_jenis' => $request->nama_kategori_jenis,
            'deskripsi' => $request->deskripsi,
            'kategori_id' => $request->id_kategori
        ]);

        return redirect()->back()->with('masuk','Data Berhasil Di Input');
    }

    public function editjenis($id)
    {
        $jenis = DB::table('kategori_jenis')->where('id_kategori_jenis',$id)->first();
        $kategori = DB::table('kategori')->where('id_kategori',$jenis->kategori_id)->first();

        return view('kategori/editjenis',compact('jenis','kategori'));
    }

    public function updatejenis(Request $request)
    {
        DB::table('kategori_jenis')->where('id_kategori_jenis',$request->id_kategori_jenis)->update([
            'nama_kategori_jenis' => $request->nama_kategori_jenis,
            'deskripsi' => $request->deskripsi
        ]);

        return redirect('kategori/detail/'.$request->kategori_id)->with('update','Data Berhasil Di Update');
    }

    public function hapusjenis($id)
    {
        $jenis_id = DB::table('kategori_jenis')->where('id_kategori_jenis',$id)->first();
        $jenis = DB::table('kategori_jenis')->where('id_kategori_jenis',$id);
        $jenis->delete();

    	return redirect('kategori/detail/'.$jenis_id->kategori_id)->with('update','Data Berhasil Di Hapus');
    }

    public function detailjenis($id)
    {
        $jenis= DB::table('kategori_jenis')->where('id_kategori_jenis',$id)->first();
        $kategori = DB::table('kategori')->where('id_kategori',$jenis->kategori_id)->first();
        $komoditas= DB::table('kategori_komoditas')->where('kategori_jenis_id',$id)->get();
        return view('kategori/detailjenis',compact('jenis','komoditas','kategori'));
    }

    public function detailjenisstore(Request $request)
    {
        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $destinationPath = \base_path() ."/public/images";
            $originalFile = $file->getClientOriginalName();
            $file->move($destinationPath, $originalFile);

            DB::table('kategori_komoditas')->insert([
                'nama_kategori_komoditas' => $request->nama_kategori_komoditas,
                'deskripsi' => $request->deskripsi,
                'kategori_jenis_id' => $request->id_kategori_jenis,
                'gambar' => $originalFile
            ]);
        }
        else{
            DB::table('kategori_komoditas')->insert([
                'nama_kategori_komoditas' => $request->nama_kategori_komoditas,
                'deskripsi' => $request->deskripsi,
                'kategori_jenis_id' => $request->id_kategori_jenis
            ]);
        }

        return redirect()->back()->with('masuk','Data Berhasil Di Input');
    }

    public function hapuskomoditas($id)
    {
        $komoditas_id = DB::table('kategori_komoditas')->where('id_kategori_komoditas',$id)->first();
        $komoditas = DB::table('kategori_komoditas')->where('id_kategori_komoditas',$id);
        $komoditas->delete();

    	return redirect('kategori/detailjenis/'.$komoditas_id->kategori_jenis_id)->with('update','Data Berhasil Di Hapus');
    }

    public function editkomoditas($id)
    {
        $komoditas = DB::table('kategori_komoditas')->where('id_kategori_komoditas',$id)->first();
        $jenis = DB::table('kategori_jenis')->where('id_kategori_jenis',$komoditas->kategori_jenis_id)->first();
        $kategori = DB::table('kategori')->where('id_kategori',$jenis->kategori_id)->first();

        return view('kategori/editkomoditas',compact('komoditas','jenis','kategori'));
    }

    public function updatekomoditas(Request $request)
    {
        if ($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $destinationPath = \base_path() ."/public/images";
            $originalFile = $file->getClientOriginalName();
            $file->move($destinationPath, $originalFile);

            DB::table('kategori_komoditas')->where('id_kategori_komoditas',$request->id_kategori_komoditas)->update([
                'nama_kategori_komoditas' => $request->nama_kategori_komoditas,
                'deskripsi' => $request->deskripsi,
                'gambar' => $originalFile
            ]);
        }
        else{
            DB::table('kategori_komoditas')->where('id_kategori_komoditas',$request->id_kategori_komoditas)->update([
                'nama_kategori_komoditas' => $request->nama_kategori_komoditas,
                'deskripsi' => $request->deskripsi
            ]);
        }

        return redirect('kategori/detailjenis/'.$request->id_kategori_jenis)->with('update','Data Berhasil Di Update');
    }

    public function detailkomoditas($id)
    {
        $komoditas= DB::table('kategori_komoditas')->where('id_kategori_komoditas',$id)->first();
        $jenis = DB::table('kategori_jenis')->where('id_kategori_jenis',$komoditas->kategori_jenis_id)->first();
        $kategori = DB::table('kategori')->where('id_kategori',$jenis->kategori_id)->first();
        return view('kategori/detailkomoditas',compact('komoditas','jenis','kategori'));
    }
}
